<?php
require 'EasyRequest.php';
function send_data($method, $url, $data=array(), $headers=array(), $auth=array()) {
    	$client = EasyRequest::create(
    		$method, 
    		$url,
    		array('headers' => $headers, 'form_params' => $data, 'auth' => $auth)
		)->send();
		return json_decode($client->getResponse()['body'], true);
}

# Simple PHP ```http``` request maker

### Required

* cURL

### Usage

```
require 'hreq/load.php';
```

``` 
send_data($method, $url, $data=array(), $headers=array(), $auth=array());
```
